package com.example.ktortest.common

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngineConfig
import io.ktor.client.engine.android.Android
import io.ktor.client.features.HttpCallValidator
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.lang.Exception

object AppHttp {
    const val BASE_API = "https://reqres.in/api/"
    val json = Json(JsonConfiguration.Stable.copy())

    private val httpConfiguration: HttpClientConfig<HttpClientEngineConfig>.() -> Unit = {
        install(JsonFeature) {
            serializer = KotlinxSerializer(json)
        }

        install(HttpCallValidator){
            validateResponse {
               if (it.status.value == 401)
                   throw Exception("Auth")
            }
        }

        install(Auth){
            basic {
                username = "user"
                password = "password"
            }
        }
    }
    val http: HttpClient = HttpClient(Android, httpConfiguration)

    suspend fun refreshToken(){

    }
}
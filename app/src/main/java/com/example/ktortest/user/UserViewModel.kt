package com.example.ktortest.user

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {
    private val repository = UserRepository()

    init {
        viewModelScope.launch {
            kotlin.runCatching { repository.loadUser(id = 2) }.fold(
                onSuccess = { user ->
                    Log.d("User", user.email)
                },
                onFailure = { throwable ->
                    Log.e("user", throwable.message ?: "Oops")
                }
            )
        }
    }
}
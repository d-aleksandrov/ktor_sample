package com.example.ktortest.user

import com.example.ktortest.common.AppHttp
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

class UserApi(private val http: HttpClient) {
    suspend fun loadUser(id: Int): User = http.get<Data>("${AppHttp.BASE_API}users/$id").user

    @Serializable
    class Data(@SerialName("data") val user: User)
}
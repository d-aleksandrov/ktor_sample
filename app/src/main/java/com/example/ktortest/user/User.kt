package com.example.ktortest.user

@kotlinx.serialization.Serializable
class User(
    val id: Int,
    val email: String
)
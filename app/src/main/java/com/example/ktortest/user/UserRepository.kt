package com.example.ktortest.user

import com.example.ktortest.common.AppHttp

class UserRepository {
    private val api = UserApi(AppHttp.http)

    suspend fun loadUser(id: Int): User = api.loadUser(id)
}